#!/bin/bash

function print_help () {
    echo "Nope"
}

if [ -z "$1" ] || [ -z "$2" ]; then
    print_help
    exit 1
else
    if [[ $1 == "--organization" || $1 == "-o" ]] ; then
        cntx="orgs"
        name=$2
    elif [[ $1 == "--user" || $1 == "-u" ]] ; then
        cntx="users"
        name=$2
    elif [[ $1 == "--maximum-page" || $1 == "-m" ]] ; then
        max=$2

        if [ -z "$3" || -z "$4"]; then
            print_help
            exit 1
        elif [[ $3 == "--organization" || $3 == "-o" ]] ; then
            cntx="orgs"
            name= $4
        elif [[ $3 == "--user" || $3 == "-u" ]] ; then
            cntx="users"
            name= $4
        else
            print_help
            exit 1
        fi
    fi
fi

if [[ $3 == "--maximum-page" || $3 == "-m" ]] ; then
    if [ -z "$4"]; then
        print_help
        exit 1
    else
        max=$4
    fi
else
    max=9
fi

page=1

echo $name
echo $max
echo $cntx
echo $page

while [ $page -lt $max ]
do
    curl "https://api.github.com/$cntx/$name/repos?page=$page&per_page=100" | jq | grep -e 'git_url*' | cut -d \" -f 4 | xargs -L1 git clone
    $page=$page+1
done

exit 0
